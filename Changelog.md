## Changelog

All notable changes to this project will be documented in this file.

The file has 3 sections

Added : For new functionalities
Fixed : For corrections
Updated : For changes and evolutions concerning existing components

For each new US, the Jira code must be provided.

Every time a new deployment in production is done, the version will be
updated based on the project rules and the date will be added.

## Example

[1.0.0-SNAPSHOT]

Added

- [Ticket-No] New feature ...

Fixed
- [Tiket-no] Bug XXX fixed

Updated

## End of example
